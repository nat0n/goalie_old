<div class="inner">
	<h2>Help</h2>
	<ul>
		<li>Sort items according to their priority by dragging them vertically up and down – high priority items at the top, lower at the bottom.</li>
		<li>Connect or discconnect them by dragging items from different columns on top of each other.</li>
		<li>Shortcuts:
			<ul>
				<li><kbd>Shift</kbd> (while pressed down) or <kbd>Caps lock</kbd> (active) shows all «nested» relations for the currently hovered item</li>
				<li><kbd>A</kbd> toggles the «About» area with information about the currently active Goalie board</li>
				<li><kbd>B</kbd> toggles this «Boards» area with a list of available Goalie boards</li>
				<li><kbd>E</kbd> opens up the editor for the currently hovered item (the one with a dimmed pencil)</li>
				<li><kbd>F</kbd> toggles between normal and «fat» mode (full height items)</li>
				<li><kbd>H</kbd> toggles the «Help» area – the one you're reading right now</li>
				<li><kbd>S</kbd> auto-sorts items within the currently hovered column, based on the amount of incoming and outgoing relations – most goes at the top</li>
				<li><kbd>U</kbd> highlights non-related items (used e.g. when you want to see what items you've forgot to link to something)</li>
			</ul>
		</li>
	</ul>
</div>