var save_timer = null, token = '', caller;

var goalie = {};

jQuery.fn.makeDroppable = function () {
	return this.each(function () {
		jQuery(this).droppable({
			accept: '.card',
			hoverClass: 'hover',
			tolerance: 'intersect',
			drop: function(event, ui) {
				// Reset styling from jQuery UI, if set somehow
				var from = ui.draggable;
				var fromName = from.attr('id');
				var to = jQuery(event.target);
				var toName = to.attr('id');

				if (from.parent()[0] != to.parent()[0]) {
					var from_id = ui.draggable[0].id;
					var to_id = jQuery(event.target).attr('id');
					if (!jQuery(event.target).hasClass(from_id) && !jQuery(ui.draggable).hasClass(to_id)) {
						// Not related yet, shall be linked
						addRelation(fromName, toName);
						jQuery(event.target).addClass(from_id + ' related');
						jQuery(ui.draggable).addClass(to_id + ' related');
						jQuery.fn.showRelations.draw(jQuery(event.target), to);
					} else {
						// Already related, shall be unlinked
						removeRelation(fromName, toName);
						jQuery(event.target).removeClass(from_id + ' related');
						jQuery(ui.draggable).removeClass(to_id + ' related');
					}
				}
			}
		});
	});
};

jQuery.fn.showRelations = {
	reset: function () {
		//clear the canvas by readjusting the width/height
		var canvasDiv = jQuery('#relations');
		var html5Canvas = canvasDiv.find('canvas');

		if (html5Canvas.length > 0) {
			html5Canvas[0].width = canvasDiv.width();
			html5Canvas[0].height = canvasDiv.height();
		}
	},
	//uses HTML5 <canvas> to draw line representing relationship
	//IE support with excanvas.js
	draw: function (from, to, thickness) {

		//draw from/to the centre, not the top left
		//don't use .position()
		//that will be relative to the parent div and not the page
		var sourceX = from.offset().left + from.outerWidth() / 2;
		var sourceY = from.offset().top + from.outerHeight() / 2;

		var targetX = to.offset().left + to.outerWidth() / 2;
		var targetY = to.offset().top + to.outerHeight() / 2;

		var canvas = jQuery('#relations__canvas');

		//you need to draw relative to the canvas not the page
		var canvasOffsetX = canvas.offset().left;
		var canvasOffsetY = canvas.offset().top;

		var context = canvas[0].getContext('2d');

		//draw line
		context.beginPath();
		context.moveTo(sourceX - canvasOffsetX, sourceY - canvasOffsetY);
		context.lineTo(targetX - canvasOffsetX, targetY - canvasOffsetY);
		context.closePath();
		//ink line
		context.lineWidth = thickness || 2;
		context.strokeStyle = "rgba(0, 0, 0, 0.3)";
		context.stroke();
	},

	drawLines: function () {
		//reset the canvas
		jQuery().showRelations.reset();

		var fromTo;
		//you must create an object that holds the start and end of the line
		//and populate a collection of them to iterate through
		fromTo.each(function (i, startEndPair) {
			//dot notation used, you will probably have a different method
			//to access these elements
			var start = startEndPair.start;
			var end = startEndPair.end;

			jQuery().showRelations.draw(start, end);
		});
	}
};

function showMessage(data) {
	jQuery('#message').text(data).slideDown().delay(1500).slideUp();
}

function autoPrioritize(elms, insertpoint) {
	// Build sort order array
	var arr = [];
	elms.each(function() {
		var card = jQuery(this);
		var obj = {};
		obj.id = card.attr('id');
		var classnames = card.attr('class');
		obj.classes = /\d{1,}/.test(classnames) ? classnames.match(/\d{1,}/g).length : 0;
		arr.push(obj);
	});
	// Sort by checking if first is bigger than previous
	arr.sort(function(obj1, obj2) {
		return obj1.classes - obj2.classes;
	});
	// Loop through order and rearrange items
	jQuery.each(arr, function(i, elm) {
		if (insertpoint.length) {
			jQuery('#'+elm.id).insertAfter(insertpoint);
		} else {
			jQuery('#'+elm.id).prependTo(elms.closest('.column'));
		}
		updatePostDate(elm.id.substr(elm.id.lastIndexOf('-') + 1), arr.length - i);
	});

}

function addRemoveDivider() {
	// TODO: Gör CPT av denna och hantera sorteringsordning på samma sätt som vanliga posts
	var divider = jQuery('<div class="divider"/>').sortable({
		axis: 'y',
		containment: 'parent'
	});
	var item = jQuery(goalie.activeItem);
	if (item.next('.divider').length) {
		item.next('.divider').remove();
	} else {
		if (!item.is(':last-child')) {
			jQuery(item).after(divider);
		}
	}
}

/* Ajax */
if (typeof WP_API_Settings === 'object') {
	jQuery.ajaxSetup({
		method: 'POST',
		beforeSend: function(xhr) {
			xhr.setRequestHeader('X-WP-Nonce', WP_API_Settings.nonce);
		},
		fail: function(data) {
			var error = data.responseJSON.message;
			alert(error);
		}
	});
} else {
	alert("Goalie is currently in read only mode, since WP REST API doesn't seem to be installed, activated or reachable at the moment.");
}

function addPost(data, category) {
	var now = new Date();
	var postId;
	jQuery.ajax({
		url: WP_API_Settings.root + 'wp/v2/posts',
		data: {
			'title': data.substr(0, 40) + (data.length > 40 ? '...' : ''),
			'content': data,
			'date': now.toISOString(),
			'modified': now.toISOString(),
			'status': 'publish'
		}
	}).done(function(data) {
		postId = data.id;
		// Add new category
		jQuery.ajax({
			url: WP_API_Settings.root + 'wp/v2/posts/' + postId + '/categories/' + category
		});
		// Remove "Uncategorized"
		jQuery.ajax({
			url: WP_API_Settings.root + 'wp/v2/posts/' + postId + '/categories/1?force=true',
			method: 'DELETE'
		});
		jQuery('#saving').attr({
			'id': category + '-' + postId,
			'data-content': data.content.raw
		}).find('.front').html(data.content.rendered).next('.back').html('<p>By: me</p><p>Date: Just now</p>');
	}).fail(function(data) {
		var error = data.responseJSON.message;
		alert(error);
		jQuery('#saving').remove();
	});
}

function updatePost(id, data) {
	jQuery.ajax({
		url: WP_API_Settings.root + 'wp/v2/posts/' + id,
		data: {
			'title': data.substr(0, 40) + (data.length > 40 ? '...' : ''),
			'content': data
		}
	}).done(function(data) {
		jQuery('[id$=' + id +']').data('content', data.content.raw).find(' .front').html(data.content.rendered);
	});
}

function updatePostDate(id, offset) {
	var now = new Date();
	now.setSeconds(now.getSeconds() - (offset * 10));
	jQuery.ajax({
		url: WP_API_Settings.root + 'wp/v2/posts/' + id,
		data: {
			'date': now.toISOString()
		}
	});
}

function deletePost(id) {
	jQuery.ajax({
		url: WP_API_Settings.root + 'wp/v2/posts/' + id + '?force=true',
		method: 'DELETE'
	}).done(function() {
		jQuery('[id$=' + id +']').remove();
	});
}

(function getTags() {
	if (typeof WP_API_Settings === 'object') {
		jQuery.ajax({
			url: WP_API_Settings.root + 'wp/v2/tags',
			method: 'GET',
			data: {
				'per_page': '1000'
			}
		}).done(function(data) {
			goalie.tags = [];
			for (var i = 0; i < data.length; i++) {
				goalie.tags.push(data[i].name, data[i].id);
			};
		});
	}
})();

function addRelation(from, to) {
	// e.g. "goal-123"
	var from_name = from.substr(0, from.lastIndexOf('-'));
	var from_id = from.substr(from.lastIndexOf('-') + 1);
	var to_name = to.substr(0, to.lastIndexOf('-'));
	var to_id = to.substr(to.lastIndexOf('-') + 1);
	var tag_id;

	// From
	if (jQuery.inArray(from, goalie.tags) < 0) {
		// Tagg finns inte än
		jQuery.ajax({
			url: WP_API_Settings.root + 'wp/v2/tags',
			data: {
				'name': from,
				'slug': from
			}
		}).done(function(data) {
			tag_id = data.id;
			goalie.tags.push(from, tag_id);
			jQuery.ajax({
				url: WP_API_Settings.root + 'wp/v2/posts/' + to_id + '/tags/' + tag_id
			});
		});
	} else {
		// Tagg finns redan (och sparas just nu på nästkommande plats i arrayen)
		tag_id = goalie.tags[jQuery.inArray(from, goalie.tags) + 1];
		jQuery.ajax({
			url: WP_API_Settings.root + 'wp/v2/posts/' + to_id + '/tags/' + tag_id
		});
	}

	// To
	if (jQuery.inArray(to, goalie.tags) < 0) {
		// Tagg finns inte än
		jQuery.ajax({
			url: WP_API_Settings.root + 'wp/v2/tags',
			data: {
				'name': to,
				'slug': to
			}
		}).done(function(data) {
			tag_id = data.id;
			goalie.tags.push(to, tag_id);
			jQuery.ajax({
				url: WP_API_Settings.root + 'wp/v2/posts/' + from_id + '/tags/' + tag_id
			});
		});
	} else {
		// Tagg finns redan (och sparas just nu på nästkommande plats i arrayen)
		tag_id = goalie.tags[jQuery.inArray(to, goalie.tags) + 1];
		jQuery.ajax({
			url: WP_API_Settings.root + 'wp/v2/posts/' + from_id + '/tags/' + tag_id
		});
	}
}

function removeRelation(from, to) {
	// TODO: Delete empty tags? Delete after "emptying" a tag?
	// e.g. "goal-123"
	var from_name = from.substr(0, from.lastIndexOf('-'));
	var from_id = from.substr(from.lastIndexOf('-') + 1);
	var to_name = to.substr(0, to.lastIndexOf('-'));
	var to_id = to.substr(to.lastIndexOf('-') + 1);
	// Tag ID stored next after tag name in goalie.tags array
	var tag_id;
	if (jQuery.inArray(from, goalie.tags) > -1) {
		tag_id = goalie.tags[jQuery.inArray(from, goalie.tags) + 1];
	}
	// From
	jQuery.ajax({
		url: WP_API_Settings.root + 'wp/v2/posts/' + to_id + '/tags/' + tag_id + '?force=true',
		method: 'DELETE'
	});
	// To
	if (jQuery.inArray(to, goalie.tags) > -1) {
		tag_id = goalie.tags[jQuery.inArray(to, goalie.tags) + 1];
	}
	jQuery.ajax({
		url: WP_API_Settings.root + 'wp/v2/posts/' + from_id + '/tags/' + tag_id + '?force=true',
		method: 'DELETE'
	});
}


/* Initiate it all */
function setup() {
	var column = jQuery('.column');
	var beenDroppedElsewhere, fromIndex, toIndex;

	column.sortable({
		distance: 10,
		cursor: 'move',
		helper: 'clone',
		tolerance: 'pointer',
		revert: '100',
		start: function(e, ui) {
			// Add z-index to ensure dragged item is always on top
			jQuery(ui.item).parent().css('z-index', '1');
			fromIndex = jQuery(this).children().index(ui.item);
			jQuery(ui.helper, ui.item).css('height', function() {
				return jQuery(this).outerHeight(true);
			});

			jQuery('.related').removeClass('related');
			jQuery.fn.showRelations.reset();
			// Move edit away for now
			jQuery('#edit').appendTo('body');
		},
		over: function(e, ui) { beenDroppedElsewhere = false; },
		out: function(e, ui) { beenDroppedElsewhere = true; },
		update: function(e, ui) {
			if (beenDroppedElsewhere) {
				// Cancel sort if item were dragged to another column
				jQuery(ui.item).parent().sortable('cancel');
			}
			var sort_id;
			column.find('.card').each(function(i, elm) {
				sort_id = jQuery(elm).attr('id').substr(jQuery(elm).attr('id').lastIndexOf('-') + 1);
				updatePostDate(sort_id, i);
			});
			var id = jQuery(ui.item).attr('id');
			var self = jQuery(ui.item).parent();
			toIndex = self.children().index(ui.item);
		},
		stop: function(e, ui) {
			// Remove z-index:1 when dragging stops (se start function)
			jQuery(ui.item).parent().removeAttr('style');
		}
	}).find('.card').makeDroppable();

	column.on('mouseover', '.card', function() {
		var self = jQuery(this).addClass('related');
		if (!jQuery('.ui-sortable-helper').length && !jQuery('html').is('.noScroll')) {
			jQuery('#edit').appendTo(self);
			var relatedClasses = jQuery('.'+self.attr('id'));
			relatedClasses.not(self).addClass('related').each(function() {
				jQuery.fn.showRelations.draw(self, jQuery(this));
			});
		}
	}).on('mouseout', '.card', function() {
		if (!jQuery('.ui-sortable-helper').length) {
			var self = jQuery(this);
			var relatedClasses = jQuery('.related');
			relatedClasses.add(self).removeClass('related');
		}
		jQuery.fn.showRelations.reset();
	});

	jQuery('#tools button').on('click', function() {
		var type = jQuery(this).attr('class').split(' ')[0];
		var category = jQuery(this).find('b').text();
		var categoryId = jQuery(this).data('termId');
		var relatedColumn = jQuery('.column.'+type+'s');
		var newCard = jQuery('<div class="card '+type+'"><div class="front"><textarea cols="36" rows="6"></textarea></div><div class="back"></div></div>');
		newCard.dialog({
			modal: true,
			title: 'Add new ' + category,
			buttons: {
				'Create': function() {
					var data = jQuery(this).find('textarea').val();
					newCard.attr('id', 'saving').find('.front').text('Saving…');
					jQuery(this).dialog('close');
					addPost(data, categoryId);
				}
			},
			close: function(ui, event) {
				if (newCard.find('textarea').is(':empty')) {
					var relatedClass = newCard.attr('id');
					jQuery('.'+relatedClass).removeClass(relatedClass);
					if(newCard.not(':only-child').length){
						newCard.remove();
					} else {
						newCard.empty();
					}
				} else {
					// Avoid setting some styling from jQuery UI
                    newCard.removeClass('ui-dialog-content').prependTo(relatedColumn).css('width','').css('height','').css('min-height','').makeDroppable();
				}
				jQuery('.ui-dialog').detach();
			},
			width: 400
		}).dialog('open');
	});

	/* Edit post */
	jQuery('<button id="edit" role="button">✎</button>').appendTo('body');
	jQuery('body').on('mouseup', '#edit', function() {
		var postIdClass = jQuery(this).parent('.card').attr('id');
		var postID = postIdClass.substr(postIdClass.lastIndexOf('-') + 1);
		var postType = postIdClass.substr(0, postIdClass.lastIndexOf('-'));
		// Move edit away for now
		jQuery('#edit').appendTo('body');
		var editor = jQuery('<div><textarea cols="36" rows="6">' + jQuery('#' + postIdClass).data('content') + '</textarea></div>');
		editor.dialog({
			modal: true,
			title: 'Edit',
			buttons: {
				'Delete': function() {
					// TODO: Hitta alla posts i WP som har taggen [postIdClass] på sig och ta bort dem från dessa
					jQuery('.'+postIdClass).removeClass(postIdClass);
					deletePost(postID);
					jQuery(this).dialog('close');
				},
				'Save': function() {
					updatePost(postID, jQuery('textarea:visible').val());
					jQuery(this).dialog('close');
				}
			},
			width: 400
		}).dialog('open');
	});

}

jQuery(function() {

	setup();

	jQuery('html:has(.logged-in)').removeClass('loading');

	// Keyboard handlers
	jQuery(document).on('keydown', function(e) {
		if (!jQuery('.ui-dialog').is(':visible')) {
			// I (Info)
			if (e.keyCode == 73 ) {
				jQuery('.related:not(:hover)').removeClass('related');
				jQuery.fn.showRelations.reset();
				jQuery(goalie.activeItem).css('height', function() {
					return jQuery(this).find('.front').outerHeight(true);
				}).addClass('flipped');
			}
		}
	}).on('keyup', function(e) {
		if (!jQuery('.ui-dialog').is(':visible')) {
			var item = jQuery(goalie.activeItem);
			// Space bar (Create divider)
			if (e.keyCode == 32) {
				addRemoveDivider(item);
			}
			// Shift or Caps lock (Show all)
			if (e.keyCode == 16 || e.keyCode == 20) {
				jQuery('.related:not(:hover)').removeClass('related');
				jQuery.fn.showRelations.reset();
			}
			// A (About)
			if (e.keyCode == 65 ) {
				jQuery('.toggle-about').trigger('click');
			}
			// B (Boards)
			if (e.keyCode == 66 ) {
				jQuery('.toggle-boards').trigger('click');
			}
			// E (Edit)
			if (e.keyCode == 69 ) {
				jQuery('#edit').trigger('mouseup');
			}
			// F (Fat mode)
			if (e.keyCode == 70 ) {
				jQuery('html').toggleClass('fat');
				jQuery('.related, .hover').removeClass('related hover');
				jQuery.fn.showRelations.reset();
			}
			// H (Help)
			if (e.keyCode == 72 ) {
				jQuery('.toggle-help').trigger('click');
			}
			// I (Info)
			if (e.keyCode == 73 ) {
				jQuery(goalie.activeItem).removeClass('flipped').removeAttr('style');
				setTimeout(function() {
					jQuery('.flipped').removeClass('flipped');
				}, 200);
			}
			// S (Sort)
			if (e.keyCode == 83) {
				var items = item.nextUntil('.divider').add(item.prevUntil('.divider')).add(item);
				var insertpoint = item.prevAll('.divider:first');
				autoPrioritize(items, insertpoint);
			}
			// U (Unrelated)
			if (e.keyCode == 85 ) {
				jQuery.fn.showRelations.reset();
				jQuery('.related').removeClass('related');
				jQuery('.card').filter(function(){
					return !/[0-9*]/.test(this.className);
				}).addClass('related');
			}
		}
	}).on('keydown', function(e) {
		// When pressing Shift or Caps lock, show nested relations
		if (e.keyCode == 16 || e.keyCode == 20) {
			jQuery(goalie.activeItem).trigger('mouseover');
			jQuery.fn.showRelations.reset();
			var self = jQuery(goalie.activeItem);

			var nextColumns = self.closest('.column').nextAll('.column');
			var nextColumnRelated = nextColumns.eq(0).find('.related');
			var lineWidth = 2;

			nextColumnRelated.each(function() {
				var self = jQuery(this).addClass('related');
				jQuery.fn.showRelations.draw(jQuery(goalie.activeItem), self, lineWidth);
				self.closest('.column').next('.column').find('.'+self.attr('id')).each(function() {
					var self2 = jQuery(this).addClass('related');
					jQuery.fn.showRelations.draw(self, self2, lineWidth);
					self2.closest('.column').next('.column').find('.'+self2.attr('id')).each(function() {
						jQuery.fn.showRelations.draw(self2, jQuery(this).addClass('related'));
					});

				});
			});
		}
	});

	// Add Show menu button
	jQuery('#sidebar').on('click', 'button', function() {
		var area = this.className.replace(/toggle-/, 'show-');
		var body = jQuery('body');
		var active = body.data('activeArea');
		if (body.hasClass(active)) {
			body.removeClass(active).data('activeArea', null);
		} else {
			body.addClass(area).data('activeArea', area);
		}
	});

});

jQuery(document).on('mouseover focus', function(e) {
	goalie.activeItem = !!e.target.className.match(/card/) ? e.target : goalie.activeItem;
})

jQuery(window).resize(function(){
	jQuery.fn.showRelations.reset();
});