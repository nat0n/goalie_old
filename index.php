<!DOCTYPE html>
<html class="loading">
<head>
	<meta charset="utf-8" />
	<title>Goalie</title>
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
	<div id="main">
	<?php if ( is_user_logged_in() ) :
		get_template_part('inc/sidebar');
	endif; ?>
	<div id="board">
		<?php if ( is_user_logged_in() ) :
			get_template_part('inc/board');
		else :
			wp_login_form( );
		endif; ?>
	</div>
	</div>
	<?php if ( !function_exists( 'rest_url' ) ) : ?>
	<div id="message">
		<p><strong>Pssst:</strong> Since you're not using WordPress 4.5 or later, you'll need to install <a href="https://wordpress.org/plugins/rest-api/" target="_blank">WP REST API</a> plugin for Goalie to work. Thanks, see you in a bit!</p>
	</div>
	<?php endif; ?>
	<div id="relations"><canvas id="relations__canvas"></canvas></div>
	<?php wp_footer(); ?>
</body>
</html>