<div id="sidebar">
	<button class="toggle-boards">Boards</button>
	<button class="toggle-about">About</button>
	<button class="toggle-help">Help</button>
	<div class="layer" id="about">
		<?php get_template_part('about'); ?>
	</div>
	<div class="layer" id="boards">
		<?php get_template_part('boards'); ?>
	</div>
	<div class="layer" id="help">
		<?php get_template_part('help'); ?>
	</div>
</div>
