<?php
/*
 * Loop through Categories and Display Posts within
 */
$post_type = 'post';
// Get id for "our" category
$goalie_cat = get_category_by_slug('goalie')->term_id;

// Gets every "category" (term) in this taxonomy to get the respective posts
$terms = get_categories( array(
	'child_of' => $goalie_cat,
	'hide_empty' => false
) );
?>

<div id="tools">
<?php foreach( $terms as $term ) : ?>
	<div class="button">
		<button class="<?php echo $term->slug; ?>" data-term-id="<?php echo $term->term_id; ?>">+ Add <b><?php echo $term->name; ?></b></button>
		<b>ℹ</b>
		<div class="button-info"><?php echo $term->description; ?></div>
	</div>
<?php endforeach; ?>
</div>

<div id="wrapper">
<?php foreach( $terms as $term ) :
	$args = array(
		'post_type' => $post_type,
		'posts_per_page' => -1,  // show all posts
		'orderby' => 'date',
		'order' => 'desc',
		'tax_query' => array(
			array(
				'taxonomy' => 'category',
				'field' => 'slug',
				'terms' => $term->slug
			)
		)

	);
	$posts = new WP_Query($args); ?>

	<div class="column <?php echo $term->slug; ?>s">
	<?php while( $posts->have_posts() ) : $posts->the_post(); ?>

		<div class="card <?php echo $term->slug;
		if (get_the_tags()) :
			foreach ( get_the_tags() as $tag ) :
				echo ' ' . $tag->name;
			endforeach;
		endif; ?>" id="<?php echo $term->slug . '-' . $post->ID; ?>" data-content="<?php echo get_the_content(); ?>" tabindex="0">
			<div class="front"><?php the_content(); ?></div>
			<div class="back">
				<p>By: <?php (get_the_author_meta('first_name')) ? the_author_meta('first_name') : the_author(); ?></p>
				<p>Created: <?php echo get_the_modified_date() . ' at ' . get_the_modified_time(); ?></p>
			</div>
		</div>

	<?php endwhile; ?>
	</div>

<?php endforeach; ?>
</div>