<div class="inner">
<?php
global $wpdb;

if ( is_multisite() ) :

	$args = array(
		'network_id' => $wpdb->siteid
	);

	$boards = wp_get_sites( $args );

	$active_board = get_current_blog_id(); ?>

	<h2>Boards</h2>
	<ul>
	<?php
	foreach ( $boards as $board ) {

		$info = get_blog_details( $board );

		switch_to_blog( $info->blog_id );

		if ( function_exists( 'has_site_icon' ) && has_site_icon( $info->blog_id ) ) :
			$icon = '<img src="' . get_site_icon_url( 80, get_stylesheet_directory_uri() . '/goalie-icon.png', $info->blog_id ) . '" alt="" width="80" height="80" />';
		endif;

		if ( $info->blog_id == $active_board ) :
			printf('<li class="board">%s<h2>%s (this one)</h2> <p>Created: %s</p></li>', $icon, $info->blogname, $info->registered);
		else :
			printf('<li class="board">%s<h2><a href="%s">%s</a></h2> <p>Created: %s</p></li>', $icon, $info->siteurl, $info->blogname, $info->registered);
		endif;

		restore_current_blog();

	} ?>
	</ul>
	<?php
else :
	printf('For multiple Goalie boards, please enable <a href="%s/wp-admin/network/setup.php">Network setup</a> and come back here later.', get_site_url());
endif;
?>
</div>