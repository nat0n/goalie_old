<?php

require 'inc/theme_update_check.php';
$MyUpdateChecker = new ThemeUpdateChecker(
    'goalie',
    'https://kernl.us/api/v1/theme-updates/5660b78504c34abe792efe0a/'
);
// $MyUpdateChecker->purchaseCode = "Earlyb1rd";


show_admin_bar( false );

function goalie_setup() {
	if (function_exists( 'rest_url' )) :
		wp_localize_script( 'wp-api', 'WP_API_Settings', array(
			'root' => esc_url_raw( rest_url() ),
			'nonce' => wp_create_nonce( 'wp_rest' )
		) );

		wp_enqueue_script( 'wp-api' );
	endif;

	if ( is_user_logged_in() ) :
		wp_enqueue_script( 'jquery-ui-core' );
		wp_enqueue_script( 'jquery-ui-dialog' );
		wp_enqueue_script( 'jquery-ui-draggable' );
		wp_enqueue_script( 'jquery-ui-droppable' );
		wp_enqueue_script( 'jquery-ui-sortable' );
		wp_enqueue_script( 'goalie', get_stylesheet_directory_uri() . '/js/goalie.js', array( 'jquery-ui-sortable' ), '1.0', true );
	endif;

	wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
	wp_enqueue_style( 'goalie-ui', get_stylesheet_directory_uri() . '/css/jquery-ui-1.11.4.custom.css', array( ) );
	wp_enqueue_style( 'goalie', get_stylesheet_directory_uri() . '/css/goalie.css', array( 'goalie-ui' ) );
}
add_action( 'wp_enqueue_scripts', 'goalie_setup' );


function goalie_populate_categories() {
	$main_term = term_exists( 'Goalie', 'category' );

	// Check for and create main category
	if ( $main_term === 0 || $main_term === null ) :
		$main_term = wp_insert_term(
			'Goalie',
			'category',
			array(
				'description' => 'This is the parent category needed for Goalie to work. (If deleted, it will be recreated upon theme activation, together with its default sub categories.)',
				'slug' => 'goalie'
			)
		);

		$main_term_id = $main_term['term_id']; // Get numeric term id

		// Check for and create sub categories
		if ( term_exists('Goal', 'category') === 0 || term_exists('Goal', 'category') === null ) :
			wp_insert_term(
				'Goal',
				'category',
				array(
					'description' => 'Goals are project goals – what this project is all about, what is hopefully attained when the project is over, and preferably a short note on how we can measure it.',
					'slug' => 'goal',
					'parent' => $main_term_id
				)
			);
		endif;
		if ( term_exists('Profile', 'category') === 0 || term_exists('Profile', 'category') === null ) :
			wp_insert_term(
				'Profile',
				'category',
				array(
					'description' => 'Profiles are the people who will ensure we can meet the project goals. They come with a name, age, a short description of their work/life situation, in the context of this project.',
					'slug' => 'profile',
					'parent' => $main_term_id
				)
			);
		endif;
		if ( term_exists('Profile goal', 'category') === 0 || term_exists('Profile goal', 'category') === null ) :
			wp_insert_term(
				'Profile goal',
				'category',
				array(
				  'description' => 'Profile goals are the needs and wants of the profiles involved. What do they need to help fulfill the Goals? What motivates them? What are their “would be awesome” kind of wishes?',
				  'slug' => 'profile-goal',
				  'parent' => $main_term_id
				)
			);
		endif;
		if ( term_exists('Task', 'category') === 0 || term_exists('Task', 'category') === null ) :
			wp_insert_term(
				'Task',
				'category',
				array(
				  'description' => 'Tasks are what needs to be done to make the profile goals a reality. Here goes all the things – small and big – that needs to be researched, written, designed and developed.',
				  'slug' => 'task',
				  'parent' => $main_term_id
				)
			);
		endif;
	endif;

}
add_action( 'after_setup_theme', 'goalie_populate_categories' );