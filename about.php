<div class="inner">
	<h2><?php bloginfo('name'); ?></h2>
	<p><?php bloginfo('description'); ?></p>
	<h3>Contributing users:</h3>
	<p><?php wp_list_authors(array(
		'show_fullname' => true,
		'optioncount' => true,
		'html' => false
	)); ?></p>
</div>